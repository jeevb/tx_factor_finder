# Transcription Factor Finder

An API to query UCSC's MySQL database for transcription factor chIP-Seq data around putative refSeq gene promoter regions.

## Project Correspondents

**Roman Camarda**  
University of California, San Francisco  

**Dai Horiuchi**  
University of California, San Francisco
