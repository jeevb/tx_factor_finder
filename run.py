import codecs
import time

from query import Query
from settings import *
from writer import Writer

def main():
    q = Query(promoter=(UPSTREAM_BOUND, DOWNSTREAM_BOUND))

    gene_records = 0

    for gene, res in q.get_results(GENES, FACTORS).items():
        print "Writing record for %s" % gene

        with codecs.open(gene + ".html", "w", encoding="utf-8") as ohandle:
            ohandle.write(Writer(gene, res).html)

        gene_records += 1

    return gene_records

if __name__ == '__main__':
    start_time = time.time()
    records = main()
    print "%i records mined in %i seconds." % (
        records,
        time.time() - start_time)
