BASE_QUERY = 'select g.name2, f.chrom, f.chromStart, f.chromEnd, f.name, f.expNums, f.expScores from refGene as g inner join wgEncodeRegTfbsClusteredV3 as f where g.chrom = f.chrom and ((f.chromStart >= if(g.strand = "+", g.txStart-{u}, g.txEnd-{d}) and f.chromStart <= if(g.strand = "+", g.txStart+{d}, g.txEnd+{u})) or (f.chromEnd >= if(g.strand = "+", g.txStart-{u}, g.txEnd-{d}) and f.chromEnd <= if(g.strand = "+", g.txStart+{d}, g.txEnd+{u})))'
GENES = ' and g.name2 in ({g})'
FACTORS = ' and f.name in ({f})'

PREPARED_STATEMENTS = {
    "exp_descs": ('select c.id, i.factor, i.antibody, i.cellType, '
        'i.treatment, i.lab from wgEncodeRegTfbsClusteredInputsV3 '
        'as i left join wgEncodeRegTfbsCellsV3 as c on '
        'i.source = c.description'),
    "all_genes" : (BASE_QUERY),
    "genes": (BASE_QUERY + GENES),
    "factors": (BASE_QUERY + FACTORS),
    "genes_factors": (BASE_QUERY + GENES + FACTORS)
}

def Q(stmt, **kwargs):
    return PREPARED_STATEMENTS[stmt].format(**kwargs)
