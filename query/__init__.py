import MySQLdb

from collections import defaultdict
from copy import copy
from formats import Experiment, Result
from itertools import izip
from statements import Q


class Query():

    def __init__(self, promoter):
        conn = MySQLdb.connect(
            host="genome-mysql.cse.ucsc.edu",
            user="genome",
            db="hg19"
        )
        self.cur = conn.cursor()

        # Bounds for defining a gene's promoter region
        # relative to the transcription start site (TSS)
        # Default is (2000, 1000) referring to a region
        # that is between 2000 bases upstream and 1000 bases
        # downstream of the TSS of a gene
        self.u, self.d = promoter

        self.exp_descs = defaultdict(list)
        self._get_exp_descs()

    @staticmethod
    def _list_to_string(a):
        return ",".join(repr(i) for i in a)

    def _get_exp_descs(self):
        print "\nExtracting experiment descriptions..."

        self.cur.execute(Q("exp_descs"))

        for exp in self.cur.fetchall():
            _exp = Experiment(exp)
            self.exp_descs[_exp.factor].append(_exp)

    def _get_exps_scores(self, result):
        result.exps = {e.id: copy(e) for e in self.exp_descs[result.factor]}

        exp_ids = result.exp_nums.split(",")
        exp_scores = result.exp_scores.split(",")

        for idx, score in izip(exp_ids, exp_scores):
            result.exps[long(idx)].score = score

    def get_results(self, genes, factors):
        print "\nRunning query..."

        if genes and factors:
            self.cur.execute(Q(
                "genes_factors",
                u=self.u,
                d=self.d,
                g=self._list_to_string(genes),
                f=self._list_to_string(factors)
            ))
        elif genes:
            self.cur.execute(Q(
                "genes",
                u=self.u,
                d=self.d,
                g=self._list_to_string(genes)
            ))
        elif factors:
            self.cur.execute(Q(
                "factors",
                u=self.u,
                d=self.d,
                f=self._list_to_string(factors)
            ))
        else:
            self.cur.execute(Q(
                "all_genes",
                u=self.u,
                d=self.d,
            ))

        results = defaultdict(set)
        query_results = set(i for i in self.cur.fetchall())

        print ("\n%i records successfully extracted...\n"
            "Sorting and organizing..." % len(query_results))

        for result in query_results:
            res = Result(result)
            self._get_exps_scores(res)

            results[res.gene].add(res)

        print "Done.\n"

        return results
