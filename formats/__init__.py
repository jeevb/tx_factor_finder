class Experiment():

    def __init__(self, exp):
        (
            self.id,
            self.factor,
            self.antibody,
            self.cell_type,
            self.treatment,
            self.lab
        ) = exp

        self.score = 0


class Result():

    def __init__(self, result):
        (
            self.gene,
            self.chrom,
            self.chrom_start,
            self.chrom_end,
            self.factor,
            self.exp_nums,
            self.exp_scores
        ) = result

        self.exps = None
