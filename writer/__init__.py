import codecs
import jinja2
import os
import sys

from markdown import markdown

GENECARDS_URL = "http://www.genecards.org/cgi-bin/carddisp.pl?gene={gene}"
UCSC_TRACK_URL = "http://genome.ucsc.edu/cgi-bin/hgTracks?db=hg19&position={chrom}:{chrom_start}-{chrom_end}"


class Writer():

    def __init__(self, gene, results):
        self.gene = gene
        self.results = sorted(results, key=lambda r: (r.factor, r.chrom_start))

    @property
    def md(self):
        text = ("## [{gene}](" + GENECARDS_URL + ")\n\n").format(
            gene=self.gene)

        factors = " ".join(['[`{factor}`](#{factor})'.format(factor=f)
            for f in sorted(set(r.factor for r in self.results))])
        text += factors + "\n\n"

        for res in self.results:
            text += ('<a name="{factor}"></a>\n' + '### [{factor}](' +
                GENECARDS_URL +
                ') [({chrom}:*{chrom_start}*-*{chrom_end}*)]' +
                '(' + UCSC_TRACK_URL + ')\n').format(
                    factor=res.factor,
                    gene=res.factor,
                    chrom=res.chrom,
                    chrom_start=res.chrom_start,
                    chrom_end=res.chrom_end
                )

            table = (
                "Antibody | Cell Type | Treatment | Lab | Score\n"
                "--- | :---: | :---: | :---: | ---:\n"
            )
            for exp in res.exps.values():
                table += (
                    "{antibody} | {cell_type} | {treatment} | {lab} | "
                    "{score}\n".format(
                    antibody=exp.antibody,
                    cell_type=exp.cell_type,
                    treatment=exp.treatment,
                    lab=exp.lab,
                    score=exp.score
                ))
            text += table + "\n\n"

        return text.replace("_", "\_")

    @property
    def html(self):
        loader = jinja2.FileSystemLoader(searchpath="/")
        env = jinja2.Environment(loader=loader)
        template_path = os.path.join(sys.path[0], "template.html")
        template = env.get_template(template_path)

        return template.render(gene=self.gene,
            results=markdown(self.md, extensions=["extra"]))
